import org.json.JSONArray;
import org.json.JSONObject;
import java.io.*;
import java.util.*;

class Pair {
    private String name;
    private Double score;

    public Pair(String name, Double score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public Double getScore() {
        return score;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return name + " " + score;
    }

    // custom method to generate a CSV record for the Pair object
    public String toCSV() {
        return name + ", " + score + "\n";
    }
}

// class used to sort the custom Pair data by score
class PairSort implements Comparator<Pair> {
    @Override
    public int compare(Pair p1, Pair p2) {
        return (int) ((p1.getScore() - p2.getScore()) * -1);
    }
}

class JSONParse {
    private List<Pair> final_data = new ArrayList<>();
    private String data_file;
    private String report_definition;
    private boolean useExprienceMultiplier;
    private int periodLimit;
    private int topPerformersThreshold;
    private JSONArray data;

    public JSONParse(String data, String report) {
        data_file = data;
        report_definition = report;
    }

    protected void generateCSV() {
        // go through each object in the json array
        for (int i = 0; i < data.length(); i++) {
            double score = 0;
            // get the whole object at that position
            JSONObject temp = data.getJSONObject(i);

            String name = (String) temp.get("name");
            int salesPeriod = (int) temp.get("salesPeriod");

            // skip if the sales period is higher than the report defined period
            if (salesPeriod > periodLimit) {
                continue;
            }

            if (!useExprienceMultiplier) {
                score = ((int) temp.get("totalSales")) / salesPeriod;
            } else {
                score = (((int) temp.get("totalSales")) / salesPeriod) * (double) temp.get("experienceMultiplier");
            }

            // add the person to the list
            final_data.add(new Pair(name, score));
        }

        Collections.sort(final_data, new PairSort());

        long ts = (new Date()).getTime();
        try {
            // create new file writer
            String file_name = ts + ".csv";
            FileWriter csvWriter = new FileWriter(file_name);

            csvWriter.append("Name");
            csvWriter.append(",");
            csvWriter.append("Score");
            csvWriter.append("\n");

            double max = final_data.get(0).getScore();
            double min = max - (final_data.get(0).getScore() * topPerformersThreshold / 100);
            for (int i = 0; i < final_data.size(); i++) {
                // check if the current person score is within the treshold
                if (final_data.get(i).getScore() > min) {
                    // if so, add the data to the writer
                    csvWriter.append(final_data.get(i).toCSV());
                }
                // otherwise break the loop
                else {
                    break;
                }
            }

            // finally flush the buffer and close the file handle and close the scanner
            csvWriter.flush();
            csvWriter.close();

            System.out.println("CSV with name " + file_name + " has been successfully created");
        } catch (IOException io) {
            System.out.println(io);
        }
    }

    protected void parse() {
        try {
            Scanner scanner = new Scanner(new File(data_file));
            // try reading the data file
            String data_from_file = scanner.useDelimiter("\\A").next();

            // parse raw data into JSON array or objects
            data = new JSONArray(data_from_file);

            scanner = new Scanner(new File(report_definition));
            // try reading data from report definition
            String report_from_file = scanner.useDelimiter("\\A").next();

            // parse raw data to JSON object
            JSONObject report = new JSONObject(report_from_file);

            useExprienceMultiplier = (boolean) report.get("useExprienceMultiplier");
            periodLimit = (int) report.get("periodLimit");
            topPerformersThreshold = (int) report.get("topPerformersThreshold");

            scanner.close(); // Put this call in a finally block
        } catch (FileNotFoundException nf) {
            System.out.println("File not found. Please check file paths and names");
        }
    }
}

public class Main {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println(
                    "The format for execution is: java Main \"path_to_data_file\" \"path_to_report_definition\"");
            System.exit(1);
        }
        JSONParse jp = new JSONParse(args[0], args[1]);
        jp.parse();
        jp.generateCSV();
    }
}
