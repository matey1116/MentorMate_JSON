# MentorMate_JSON
## How to compile
Use the Java compiler to compile
```bash
javac Main.java
```

## How to run
```bash
java Main "path_to_data_file" "path_to_report_definition"
```
## License
[MIT](https://choosealicense.com/licenses/mit/)
